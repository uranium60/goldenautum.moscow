<?php

/*-----------------------------------------------------------------------------------*/
/* Options Framework Functions
/*-----------------------------------------------------------------------------------*/

/* Set the file path based on whether the Options Framework is in a parent theme or child theme */

if ( STYLESHEETPATH == TEMPLATEPATH ) {
	define('OF_FILEPATH', TEMPLATEPATH);
	define('OF_DIRECTORY', get_template_directory_uri());
} else {
	define('OF_FILEPATH', STYLESHEETPATH);
	define('OF_DIRECTORY', get_stylesheet_directory_uri());
}

/* These files build out the options interface.  Likely won't need to edit these. */

require_once (OF_FILEPATH . '/admin/admin-functions.php');		// Custom functions and plugins
require_once (OF_FILEPATH . '/admin/admin-interface.php');		// Admin Interfaces (options,framework, seo)

/* These files build out the theme specific options and associated functions. */

require_once (OF_FILEPATH . '/admin/theme-options.php'); 		// Options panel settings and custom settings
require_once (OF_FILEPATH . '/functions/theme-functions.php'); 	// Theme actions based on options settings
require_once (OF_FILEPATH . '/functions/metabox.php'); 	
require_once (OF_FILEPATH . '/functions/post-types.php'); 	
require_once (OF_FILEPATH . '/functions/theme-widgets.php'); 	
require_once (OF_FILEPATH . '/functions/shortcodes.php');
require_once (OF_FILEPATH . '/admin/tinymce/shortcodes-generator.php');
require_once (OF_FILEPATH . '/functions/breadcrumbs-plus.php');
require_once (OF_FILEPATH . '/functions/aq_resizer.php');

// Load static framework options pages 
$functions_path = OF_FILEPATH . '/admin/';

function optionsframework_add_admin() {

    global $query_string;
    
    $themename =  get_option('of_themename');      
    $shortname =  get_option('of_shortname'); 
   
    if ( isset($_REQUEST['page']) && $_REQUEST['page'] == 'optionsframework' ) {
		if (isset($_REQUEST['of_save']) && 'reset' == $_REQUEST['of_save']) {
			$options =  get_option('of_template'); 
			of_reset_options($options,'optionsframework');
			header("Location: admin.php?page=optionsframework&reset=true");
			die;
		}
    }
		
    //$of_page = add_submenu_page('themes.php', $themename, 'Theme Options', 'edit_theme_options', 'optionsframework','optionsframework_options_page'); // Default
    $of_page = add_menu_page($themename." Options", $themename, 'edit_themes', 'optionsframework', 'optionsframework_options_page');
	
	// Add framework functionaily to the head individually
	add_action("admin_print_scripts-$of_page", 'of_load_only');
	add_action("admin_print_styles-$of_page",'of_style_only');
} 

add_action('admin_menu', 'optionsframework_add_admin');

function language_selector_flags(){
    $languages = icl_get_languages('skip_missing=0&orderby=code');
    if(!empty($languages)){
        foreach($languages as $l){
            if(!$l['active']) echo '<a href="'.$l['url'].'">';
            echo '<img src="'.$l['country_flag_url'].'" height="42" alt="'.$l['language_code'].'" width="41" />';
            if(!$l['active']) echo '</a>';
        }
    }
}

function cropHomeBox($str){
global $wp;
	$current_url = add_query_arg( $wp->query_string, '', home_url( $wp->request ) );
	$findme   = '/en';
	$pos = strpos($current_url, $findme);

	if ($pos==true){

	//	echo "true";
	return end(explode('|', $str));
	}
	else
	{
		//echo "false";
	return current(explode('|', $str));
	}
}

// Создаем виджет ECOBIZ Text
class ecobiztext_widget extends WP_Widget {

function __construct() {
parent::__construct(
// Выбираем ID для своего виджета
'ecobiztext_widget', 

// Название виджета, показано в консоли
__('ECOBIZ Text Widget', 'ecobiztext_widget_domain'), 

// Описание виджета
array( 'description' => __( 'Текст или HTML', 'ecobiztext_widget_domain' ), ) 
);
}

// Создаем код для виджета - 
// сначала небольшая идентификация
	function widget( $args, $instance ) {
		extract($args);

		/** This filter is documented in wp-includes/default-widgets.php */
		$title = cropHomeBox(apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base ));

		/**
		 * Filter the content of the Text widget.
		 *
		 * @since 2.3.0
		 *
		 * @param string    $widget_text The widget content.
		 * @param WP_Widget $instance    WP_Widget instance.
		 */
		$text = cropHomeBox(apply_filters( 'widget_text', empty( $instance['text'] ) ? '' : $instance['text'], $instance ));
		echo $before_widget;
		if ( !empty( $title ) ) { echo $before_title . $title . $after_title; } ?>
			<div class="textwidget"><?php echo !empty( $instance['filter'] ) ? wpautop( $text ) : $text; ?></div>
		<?php
		echo $after_widget;
	}

		
// Закрываем код виджета
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		if ( current_user_can('unfiltered_html') )
			$instance['text'] =  $new_instance['text'];
		else
			$instance['text'] = stripslashes( wp_filter_post_kses( addslashes($new_instance['text']) ) ); // wp_filter_post_kses() expects slashed
		$instance['filter'] = isset($new_instance['filter']);
		return $instance;
	}

	function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'text' => '' ) );
		$title = strip_tags($instance['title']);
		$text = esc_textarea($instance['text']);
?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>

		<textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>"><?php echo $text; ?></textarea>

		<p><input id="<?php echo $this->get_field_id('filter'); ?>" name="<?php echo $this->get_field_name('filter'); ?>" type="checkbox" <?php checked(isset($instance['filter']) ? $instance['filter'] : 0); ?> />&nbsp;<label for="<?php echo $this->get_field_id('filter'); ?>"><?php _e('Automatically add paragraphs'); ?></label></p>
<?php
	}


	


} // Закрываем класс btru_widget

// Регистрируем и запускаем виджет
function ecobiztext_load_widget() {
	register_widget( 'ecobiztext_widget' );
}
add_action( 'widgets_init', 'ecobiztext_load_widget' );



// Создаем виджет ECOBIZ NavMenu
class ecobizmenu_widget extends WP_Widget {

function __construct() {
parent::__construct(
// Выбираем ID для своего виджета
'ecobizmenu_widget', 

// Название виджета, показано в консоли
__('ECOBIZ Custom Menu Widget', 'ecobizmenu_widget_domain'), 

// Описание виджета
array( 'description' => __( 'Произвольное меню на сайте', 'ecobizmenu_widget_domain' ), ) 
);
}

// Создаем код для виджета - 
// сначала небольшая идентификация
	function widget($args, $instance) {
		// Get menu
		$nav_menu = ! empty( $instance['nav_menu'] ) ? wp_get_nav_menu_object( $instance['nav_menu'] ) : false;

		if ( !$nav_menu )
			return;

		/** This filter is documented in wp-includes/default-widgets.php */
		$instance['title'] = cropHomeBox(apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base ));

		echo $args['before_widget'];

		if ( !empty($instance['title']) )
			echo $args['before_title'] . $instance['title'] . $args['after_title'];

		wp_nav_menu( array( 'fallback_cb' => '', 'menu' => $nav_menu ) );

		echo $args['after_widget'];
	}

		
// Закрываем код виджета
	function update( $new_instance, $old_instance ) {
		$instance['title'] = strip_tags( stripslashes($new_instance['title']) );
		$instance['nav_menu'] = (int) $new_instance['nav_menu'];
		return $instance;
	}


	function form( $instance ) {
		$title = isset( $instance['title'] ) ? $instance['title'] : '';
		$nav_menu = isset( $instance['nav_menu'] ) ? $instance['nav_menu'] : '';

		// Get menus
		$menus = wp_get_nav_menus( array( 'orderby' => 'name' ) );

		// If no menus exists, direct the user to go and create some.
		if ( !$menus ) {
			echo '<p>'. sprintf( __('No menus have been created yet. <a href="%s">Create some</a>.'), admin_url('nav-menus.php') ) .'</p>';
			return;
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $title; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('nav_menu'); ?>"><?php _e('Select Menu:'); ?></label>
			<select id="<?php echo $this->get_field_id('nav_menu'); ?>" name="<?php echo $this->get_field_name('nav_menu'); ?>">
				<option value="0"><?php _e( '&mdash; Select &mdash;' ) ?></option>
		<?php
			foreach ( $menus as $menu ) {
				echo '<option value="' . $menu->term_id . '"'
					. selected( $nav_menu, $menu->term_id, false )
					. '>'. esc_html( $menu->name ) . '</option>';
			}
		?>
			</select>
		</p>
		<?php
	}


	


} // Закрываем класс btru_widget

// Регистрируем и запускаем виджет
function ecobizmenu_load_widget() {
	register_widget( 'ecobizmenu_widget' );
}
add_action( 'widgets_init', 'ecobizmenu_load_widget' );


?>