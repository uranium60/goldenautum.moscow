<?php

/*-----------------------------------------------------------------------------------*/
/* Head Hook
/*-----------------------------------------------------------------------------------*/

function of_head() { do_action( 'of_head' ); }

/*-----------------------------------------------------------------------------------*/
/* Add default options after activation */
/*-----------------------------------------------------------------------------------*/
if (is_admin() && isset($_GET['activated'] ) && $pagenow == "themes.php" ) {
	//Call action that sets
	add_action('admin_head','of_option_setup');
}

function of_option_setup(){

	//Update EMPTY options
	$of_array = array();
	add_option('of_options',$of_array);

	$template = get_option('of_template');
	$saved_options = get_option('of_options');
	
	foreach($template as $option) {
		if($option['type'] != 'heading'){
			if (isset($option['id'])) {
			   $id = $option['id'];
			}
			if (isset($option['std'])) {
			   $std = $option['std'];
			}
			$db_option = get_option($id);
			if(empty($db_option)){
				if(is_array($option['type'])) {
					foreach($option['type'] as $child){
						$c_id = $child['id'];
						$c_std = $child['std'];
						update_option($c_id,$c_std);
						$of_array[$c_id] = $c_std; 
					}
				} else {
					update_option($id,$std);
					$of_array[$id] = $std;
				}
			}
			else { //So just store the old values over again.
				$of_array[$id] = $db_option;
			}
		}
	}
	update_option('of_options',$of_array);
}

/*-----------------------------------------------------------------------------------*/
/* Admin Backend */
/*-----------------------------------------------------------------------------------*/
function optionsframework_admin_head() { 
	
	//Tweaked the message on theme activate
	?>
    <script type="text/javascript">
    jQuery(function(){
	  var message = '<p>This theme comes with an <a href="<?php echo admin_url('admin.php?page=optionsframework'); ?>">options panel</a> to configure settings. This theme also supports widgets, please visit the <a href="<?php echo admin_url('widgets.php'); ?>">widgets settings page</a> to configure them.</p>';
    	jQuery('.themes-php #message2').html(message);
    
    });
    </script>
    <?php
}


if(is_admin()){
  add_action('admin_head', 'optionsframework_admin_head'); 
	add_action('admin_init', 'add_admin_scripts');
}


function add_admin_scripts() {
  wp_enqueue_script( 'shortcodes', get_template_directory_uri() . '/admin/tinymce/shortcodelocalization.js');
	wp_localize_script( 'shortcodes', 'objectL10n', array(
  'columns_title' => __('Columns','ecobiz'),
  'columns_inner_title' => __('Inner Columns','ecobiz'),
  'elements_title' => __('Elements','ecobiz'),
  'list_title' => __('List Style','ecobiz'),
  'onefourth_title' => __('One Fourth','ecobiz'),
  'onefourth_last_title' => __('One Fourth Last','ecobiz'),
  'onethird_title' => __('One Third','ecobiz'),
  'onethird_last_title' => __('One Third Last','ecobiz'),
  'onehalf_title' => __('One Half','ecobiz'),
  'onehalf_last_title' => __('One Half Last','ecobiz'),
  'twothird_title' => __('Two Third','ecobiz'),
  'threefourth_title' => __('Three Fourth','ecobiz'),
  'onefifth_title' => __('One Fifth','ecobiz'),
  'onefifth_last_title' => __('One Fifth Last','ecobiz'),
  'onefourth_inner_title' => __('One Fourth','ecobiz'),
  'onefourth_inner_last_title' => __('One Fourth Last','ecobiz'),
  'onethird_inner_title' => __('One Third Inner','ecobiz'),
  'onethird_inner_last_title' => __('One Third Inner Last','ecobiz'),
  'onehalf_inner_title' => __('One Half  Inner','ecobiz'),
  'onehalf_inner_last_title' => __('One Half Inner Last','ecobiz'),
  'twothird_inner_title' => __('Two Third Inner ','ecobiz'),
  'threefourth_inner_title' => __('Three Fourth Inner ','ecobiz'),
  'dropcap_title' => __('Dropcap','ecobiz'),
  'pullquote_left_title' => __('Pullquote Left','ecobiz'),
  'pullquote_right_title' => __('Pullquote Right','ecobiz'),
  'divider_title' => __('Divider','ecobiz'),
  'spacer_title' => __('Spacer','ecobiz'),
  'tabs_title' => __('Tabs','ecobiz'),
  'toggle_title' => __('Toggle','ecobiz'),
  'image_title' => __('Image','ecobiz'),
  'testimonial_title' => __('Testimonial Block','ecobiz'),
  'gmap_title' => __('Google Map','ecobiz'),
  'youtube_title' => __('Youtube','ecobiz'),
  'vimeo_title' => __('Vimeo','ecobiz'),
  'button_title' => __('Buttons','ecobiz'),
  'bigbutton_title' => __('Big Buttons','ecobiz'),
  'bulletlist_title' => __('Bullet List','ecobiz'),
  'starlist_title' => __('Star List','ecobiz'),
  'arrowlist_title' => __('Arrow List','ecobiz'),
  'itemlist_title' => __('Item List','ecobiz'),
  'checklist_title' => __('Check List','ecobiz'),
  'infobox_title' => __('Info Box','ecobiz'),
  'successbox_title' => __('Success Box','ecobiz'),
  'warningbox_title' => __('Warning Box','ecobiz'),
  'errorbox_title' => __('Error Box','ecobiz'),
	
	));

}

	
	
?>