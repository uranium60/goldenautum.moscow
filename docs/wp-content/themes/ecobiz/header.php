<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>"  />
<title><?php if (is_home () ) { echo cropHomeBox(get_bloginfo('name')); echo " - "; echo cropHomeBox(get_bloginfo('description')); 
} elseif (is_category() ) {single_cat_title(); echo " - "; echo cropHomeBox(get_bloginfo('name'));
} elseif (is_single() || is_page() ) {single_post_title(); echo " - "; echo cropHomeBox(get_bloginfo('name'));
} elseif (is_search() ) {echo cropHomeBox(get_bloginfo('name')); echo " search results: "; echo esc_html($s);
} else { wp_title('',true); }?></title>
<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
<meta name="robots" content="follow, all" />
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php $favico = get_option('ecobiz_custom_favicon');?>
<link rel="shortcut icon" href="<?php echo ($favico) ? $favico : get_template_directory_uri().'/images/favicon.ico';?>"/>
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_head(); ?>


</head>
<body <?php body_class(''); ?>>
  <div id="wrapper">
    <div id="topwrapper"></div>
    <div id="mainwrapper">
      <!-- Header Start -->
      <div id="header">
        <div class="center">
          <!-- Logo Start -->
          <div id="logo">
          <?php $logo = get_option('ecobiz_logo'); ?>
          <a href="<?php echo home_url();?>"><img src="<?php echo ($logo) ? $logo : get_template_directory_uri().'/images/logo.png';?>" alt="Logo" width="215"/></a>
          </div>
          <!-- Logo End -->

	<div id="flags_language_selector">
		<?php language_selector_flags(); ?>
             <a href="https://goldenautumn2015.expoglobus.com/egvs/Forum/EditContact.aspx"><img src="http://goldenautumn.moscow/wp-content/uploads/2014/07/ico6.png" height="42" alt="en" width="41" /></a>
	</div>    
      
          <div id="headerright">
            <!-- Menu Navigation Start --> 
            <div id="mainmenu">
              <div id="myslidemenu" class="jqueryslidemenu">
                <?php 
                if (function_exists('wp_nav_menu')) { 
                  wp_nav_menu( array( 'menu_class' => '', 'theme_location' => 'topnav', 'fallback_cb'=>'imediapixel_topmenu_pages','depth' =>4 ) );
                } 
                ?>
              </div>
            </div>
            <!-- Menu Navigation End -->
          </div>
        </div>   
      </div>
      <!-- Header End  -->