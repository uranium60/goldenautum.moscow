        <?php 
    
          $site_overview_title1 = cropHomeBox(get_option('ecobiz_site_overview_title1')); 
          $site_overview_url1   = get_option('ecobiz_site_overview_url1');
          $site_overview_icon1  = get_option('ecobiz_site_overview_icon1');
          $site_overview_desc1  = cropHomeBox(get_option('ecobiz_site_overview_desc1'));
          $site_overview_title2 = cropHomeBox(get_option('ecobiz_site_overview_title2')); 
          $site_overview_url2   = get_option('ecobiz_site_overview_url2');
          $site_overview_icon2  = get_option('ecobiz_site_overview_icon2');
          $site_overview_desc2  = cropHomeBox(get_option('ecobiz_site_overview_desc2'));
          $site_overview_title3 = cropHomeBox(get_option('ecobiz_site_overview_title3')); 
          $site_overview_url3   = cropHomeBox(get_option('ecobiz_site_overview_url3'));
          $site_overview_icon3  = get_option('ecobiz_site_overview_icon3');
          $site_overview_desc3  = cropHomeBox(get_option('ecobiz_site_overview_desc3'));
          $site_overview_title4 = cropHomeBox(get_option('ecobiz_site_overview_title4')); 
          $site_overview_url4   = get_option('ecobiz_site_overview_url4');
          $site_overview_icon4  = get_option('ecobiz_site_overview_icon4');
          $site_overview_desc4  = cropHomeBox(get_option('ecobiz_site_overview_desc4'));                  
        ?>          
          
          <!-- Content Box #1 -->
          <div class="mainbox">
            <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Homepage Box 1')) { ?>
              <h4><?php echo ($site_overview_title1) ? stripslashes($site_overview_title1) :  "";?></h4>
              <?php if ($site_overview_icon1 !="") { ?>
                <img src="<?php echo $site_overview_icon1;?>" class="alignleft" alt="" />
              <?php } ?>
              <p><?php echo $site_overview_desc1 ? stripslashes($site_overview_desc1) : "";?></p>	
            <?php } ?>
          </div>

          
          <!-- Content Box #2 -->
          <div class="mainbox box-last">
            <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Homepage Box 2')) { ?>
              <h4><?php echo $site_overview_title2 ? stripslashes($site_overview_title2) : "";?></h4>
              <?php if ($site_overview_icon2 !="") { ?>
              <img src="<?php echo $site_overview_icon2;?>" class="alignleft" alt="" />
              <?php } ?>
              <p><?php echo $site_overview_desc2 ? stripslashes($site_overview_desc2) : "";?></p>
            <?php } ?>
          </div>
          
          <div class="spacer"></div>

          <!-- Key Box #1 -->
          <div class="mainbox">
            <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Homepage Box 1')) { ?>
	<a href="<?php echo $site_overview_url1 ? $site_overview_url1 :"#";?>"><span id="mainbox_key"><span class="mainbox_key_text"><? echo cropHomeBox('Подробнее|Read more'); ?></span></span></a>
            <?php } ?>
          </div>

          <!-- Key Box #2 -->
          <div class="mainbox box-last">
            <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Homepage Box 2')) { ?>
	<a href="<?php echo $site_overview_url2 ? $site_overview_url2 :"#";?>"><span id="mainbox_key"><span class="mainbox_key_text"><? echo cropHomeBox('Подробнее|Read more'); ?></span></span></a>
            <?php } ?>
          </div>

          <div class="spacer2"></div>
          
          <!-- Content Box #3 -->
          <div class="mainbox">
            <h4><?php echo $site_overview_title3 ? stripslashes($site_overview_title3) : "";?></h4>
            <?php if ($site_overview_icon3 !="") { ?>
            <img src="<?php echo $site_overview_icon3;?>" class="alignleft" alt="" />
            <?php } ?>
            <p><?php echo $site_overview_desc3 ? stripslashes($site_overview_desc3) : "";?></p>
          </div>
          
          <!-- Content Box #4 -->
          <div class="mainbox box-last">
            <h4><?php echo $site_overview_title4 ? stripslashes($site_overview_title4) : "";?></h4>
            <?php if ($site_overview_icon4 !="") { ?>
              <img src="<?php echo $site_overview_icon4;?>" class="alignleft" alt="" />
            <?php } ?>
            <p><?php echo $site_overview_desc4 ? stripslashes($site_overview_desc4) : "";?></p>
          </div>

          <div class="spacer"></div>

          <!-- Key Box #3 -->
          <div class="mainbox">
	<a href="<?php echo $site_overview_url3 ? $site_overview_url3 :"#";?>" target="_blank"><span id="mainbox_key"><span class="mainbox_key_text"><? echo cropHomeBox('Подробнее|Read more'); ?></span></span></a>
          </div>

          <!-- Key Box #4 -->
          <div class="mainbox box-last">
	<a href="<?php echo $site_overview_url4 ? $site_overview_url4 :"#";?>"><span id="mainbox_key"><span class="mainbox_key_text"><? echo cropHomeBox('Подробнее|Read more'); ?></span></span></a>
          </div>

	<div class="spacer2"></div>

 	<!-- Content Box #5 -->
          <div class="mainbox">
            <h4><?php echo cropHomeBox("Гостиницы и трансфер|Housing and Travel"); ?></h4>
            <?php if ($site_overview_icon4 !="") { ?>
              <img src="<?php echo $site_overview_icon4;?>" class="alignleft" alt="" />
            <?php } ?>
            <p><?php echo cropHomeBox("Забронировать отель, заказать трансфер|To book a hotel and transportation"); ?></p>
          </div>

	<div class="spacer"></div>

	 <!-- Key Box #5 -->
          <div class="mainbox">
	<a href="http://goldenautumn.moscow/booking"><span id="mainbox_key"><span class="mainbox_key_text"><? echo cropHomeBox('Подробнее|Read more'); ?></span></span></a>
          </div>

