
//-----------Document ready----------

jQuery(document).ready(function($) {

	$ = jQuery.noConflict();

	$('textarea').focus(function() {
		$(this).select();
	});

	//add active class to current gallery item in accordion
	$('#galleries-accordion > li > div[id="'+$('input[name="selected_gallery"]').val()+'"]').addClass('active');

	//show active lightbox options
	$('table#lightbox-options tbody.active').show();

	//show active thumbnails selection options
	$('select[name="album_selection"]').parent().parent().nextAll(':lt(5)').hide();
	$('table tr.active').show();

	//submit form with selected gallery
	$('#galleries-accordion > li > div, .fg-paste-options').click(function() {
		var $this = $(this);
		if($this.is('a')) {
			if( $this.parent().parent().attr('id') != $('input[name="selected_gallery"]').val() && $(document).find('.fg-disable-links').size() == 0 ) {
				$('input[name="overwrite_gallery"]').val($this.parent().parent().attr('id'));
				$('#fg-options-form').submit();
			}
		}
		else {
			if( $this.attr('id') != $('input[name="selected_gallery"]').val() ) {
				$('input[name="selected_gallery"]').val($this.attr('id'));
				$('#fg-options-form').submit();
			}
		}

		return false;
	});

	//show selected lightbox options
	$('input[name="gallery"]').change(function() {
		$('#gallery-options').children('tbody').addClass('hidden').filter('#'+this.value).removeClass('hidden');

	});

	//show selected thumbnail hover effect options
	$('input[name="thumbnail_hover_effect"]').change(function() {
		$('#hover-effects-options').children('tbody').addClass('hidden').filter('#'+this.value).removeClass('hidden');
	});

	//show selected lightbox options
	$('select[name="album_selection"]').change(function() {

		if($(this).val() == 'thumbnails') {
			$(this).parent().parent().nextAll(':lt(5)').addClass('active').show();
		}
		else {
			$(this).parent().parent().nextAll(':lt(5)').removeClass('active').hide();
		}

	});

	$('.remove-image').click(function(evt) {
		$(this).parent().children('input').val('');
		_checkUploadInputs();
		return false;
	});


});