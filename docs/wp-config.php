<?php
/**
 * Основные параметры WordPress.
 *
 * Этот файл содержит следующие параметры: настройки MySQL, префикс таблиц,
 * секретные ключи, язык WordPress и ABSPATH. Дополнительную информацию можно найти
 * на странице {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Кодекса. Настройки MySQL можно узнать у хостинг-провайдера.
 *
 * Этот файл используется сценарием создания wp-config.php в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать этот файл
 * с именем "wp-config.php" и заполнить значения.
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'goldenautu_db');

/** Имя пользователя MySQL */
define('DB_USER', 'goldenautu_mysql');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'Rb_iG2og');

/** Имя сервера MySQL */
define('DB_HOST', 'goldenautu.mysql');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется снова авторизоваться.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ihAge(}_T,uIY#iNB7([g-zKt3<v(Y86;rnkk^qZ7~8<mK`S057;+#6)I&tV VAZ');
define('SECURE_AUTH_KEY',  '9#MUM7VOOn#p{8>e-#;|X|)b|gp3R~MEj<*-/z_xRTcjfK3KHoym8D:bEMB5t}K)');
define('LOGGED_IN_KEY',    'Sn-MA,TCJMcW{=>3N^o-~?y%GA%+{@8HFYaJ14BvrBxy8}36so0)okEKC(s4<Hpd');
define('NONCE_KEY',        '-Kb)h1/34|sFv*75_8k|`!fKv) z9Tnz/a V^-emtGq9#g{h;q>xj.1swv+Ba>1+');
define('AUTH_SALT',        'a3*!aEkx[]QTb+E.cg[Y[*L+c$Ed.-?5V{iG^VZOg>uviu+[m!-.Wlciq1&@<ZU}');
define('SECURE_AUTH_SALT', 'VUo3=`u]wM^!u?oM*%~^KLqgDA&OnCnV!55?`&r~*_1~cRBn4$Z3W}$hM.|WAqPc');
define('LOGGED_IN_SALT',   ':(SQ@Yw~XQ}v+8_ $JIhK+Abm6EYmQ-,A|fWY]EG/hO+Gc}(e)j-5?iD9#pJX0o5');
define('NONCE_SALT',       'XE7|.(OW~|o>#CqHz$_q){Bwa/lth]>Ea__lzJ[^wt})CtEBuy}XO0~+^/kz8Q#O');
/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько блогов в одну базу данных, если вы будете использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wpgold_';

/**
 * Язык локализации WordPress, по умолчанию английский.
 *
 * Измените этот параметр, чтобы настроить локализацию. Соответствующий MO-файл
 * для выбранного языка должен быть установлен в wp-content/languages. Например,
 * чтобы включить поддержку русского языка, скопируйте ru_RU.mo в wp-content/languages
 * и присвойте WPLANG значение 'ru_RU'.
 */
define('WPLANG', 'ru_RU');

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Настоятельно рекомендуется, чтобы разработчики плагинов и тем использовали WP_DEBUG
 * в своём рабочем окружении.
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
